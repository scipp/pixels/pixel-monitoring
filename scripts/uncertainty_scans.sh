#tune/scan in units of electrons
minTunePar=500
maxTunePar=10000
stepTunePar=500
minScanPar=500
maxScanPar=20000
stepScanPar=500

# $1=min, $2=max, $3=step
function get_threshold_tune() {
  local min="${1:-0}"
  local max="${2:-500}"
  local step="${3:-16}"
  jq '.scan.loops = (.scan.loops | map(if .loopAction=="Rd53aGlobalFeedback" then .config={"parameter": "DiffVth1", "step": '"$step"', "min": '"$min"', "max": '"$max"', "pixelRegs": [0, 1]} else . end))' $PIXELMON_ROOT/Yarr/src/configs/scans/rd53a/diff_tune_globalthreshold.json > diff_tune_globalthreshold.json
}

# $1=min, $2=max, $3=step
function get_threshold_scan() {
  local min="${1:-50}"
  local max="${2:-200}"
  local step="${3:-5}"
  jq '.scan.loops = (.scan.loops | map(if .loopAction=="Rd53aParameterLoop" then .config={"parameter": "InjVcalDiff", "step": '"$step"', "min": '"$min"', "max": '"$max"'} else . end))' $PIXELMON_ROOT/Yarr/src/configs/scans/rd53a/std_thresholdscan.json > std_thresholdscan.json
}

common_args=(-r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json)

#minThresh/maxThresh in units of Vcal (~=10 electrons)
minTune=0
maxTune=500
minThresh=0
maxThresh=200
for tuneThreshold in $(seq $minTunePar $stepTunePar $maxTunePar)
do
  echo "[INFO] Tuning to ${tuneThreshold}e- (minTunePar=${minTunePar}, stepTunePar=${stepTunePar}, maxTunePar=${maxTunePar})"
  # generate the necessary tune/scan config files
  echo "[INFO]   get_threshold_tune ${minTune} ${maxTune} 16"
  get_threshold_tune $minTune $maxTune 16
  echo "[INFO]   get_threshold_scan ${minScan} ${maxScan} 5"
  get_threshold_scan $minThresh $maxThresh 5
  build/bin/scanConsole "${common_args[@]}" -s Yarr/src/configs/scans/rd53a/std_digitalscan.json -p -m 1
  build/bin/scanConsole "${common_args[@]}" -s diff_tune_globalthreshold.json -p -t $tuneThreshold
  build/bin/scanConsole "${common_args[@]}" -s std_thresholdscan.json -p
  for scanThreshold in $(seq $minScanPar $stepScanPar $maxScanPar)
  do
    echo "[INFO]   Scanning with injection at ${scanThreshold}e- (minScanPar=${minScanPar}, stepScanPar=${stepScanPar}, maxScanPar=${maxScanPar})"
    build/bin/scanConsole "${common_args[@]}" -s Yarr/src/configs/scans/rd53a/std_totscan.json -p -t $scanThreshold
  done
  addTo=$(( stepTunePar/10 ))
  minTune=$(( minTune+addTo ))
  maxTune=$(( maxTune+addTo ))
  if (( $tuneThreshold > 2500 )); then
    minThresh=$(( minThresh+addTo ))
  fi
  maxThresh=$(( maxThresh+addTo ))
done
