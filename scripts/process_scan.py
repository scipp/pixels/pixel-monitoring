import glob
import json
import os
import shutil

def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)

def move_pdfs(foldname, destination):
  print('Copying files in {0:s} to {1:s}'.format(foldname, destination))
  files_to_move = {
      'std_digitalscan': ['JohnDoe_0_OccupancyMap_DIFF.pdf',
                          'JohnDoe_0_OccupancyMap_LIN.pdf',
                          'JohnDoe_0_OccupancyMap_STACK.pdf',
                          'JohnDoe_0_OccupancyMap_SYN.pdf',
      ],
      'std_analogscan': ['JohnDoe_0_OccupancyMap_DIFF.pdf',
                         'JohnDoe_0_OccupancyMap_LIN.pdf',
                         'JohnDoe_0_OccupancyMap_STACK.pdf',
                         'JohnDoe_0_OccupancyMap_SYN.pdf',
      ],
      'std_thresholdscan': ['JohnDoe_0_ThresholdMap-0_All.pdf',
                            'JohnDoe_0_ThresholdMap-0_DIFF.pdf',
                            'JohnDoe_0_ThresholdMap-0_DIFFrmsrange.pdf',
                            'JohnDoe_0_ThresholdMap-0_LIN.pdf',
                            'JohnDoe_0_ThresholdMap-0_LINrmsrange.pdf',
                            'JohnDoe_0_ThresholdMap-0_PLOT.pdf',
                            'JohnDoe_0_ThresholdMap-0_STACK.pdf',
                            'JohnDoe_0_ThresholdMap-0_STACKrmsrange.pdf',
                            'JohnDoe_0_ThresholdMap-0_SYN.pdf',
                            'JohnDoe_0_ThresholdMap-0_SYNrmsrange.pdf',
                            'JohnDoe_0_TimePerFitDist-0.pdf',
                            'JohnDoe_0_sCurve-0_PLOT.pdf',
      ],
      'std_totscan': ['JohnDoe_0_MeanTotMap0_All.pdf',
                      'JohnDoe_0_MeanTotMap0_DIFF.pdf',
                      'JohnDoe_0_SigmaTotMap0_DIFF.pdf',
                      'JohnDoe_0_MeanTotMap0_LIN.pdf',
                      'JohnDoe_0_SigmaTotMap0_LIN.pdf',
                      'JohnDoe_0_MeanTotMap0_PLOT.pdf',
                      'JohnDoe_0_MeanTotMap0_STACK.pdf',
                      'JohnDoe_0_SigmaTotMap0_STACK.pdf',
                      'JohnDoe_0_MeanTotMap0_SYN.pdf',
                      'JohnDoe_0_SigmaTotMap0_SYN.pdf',
      ],
  }
  for pattern, files in files_to_move.items():
    if not pattern in foldname: continue
    for fname in files:
      src = os.path.join(foldname, fname)
      dest = os.path.join(destination, fname)
      print('\tCopying {0:s} to {1:s}'.format(src, dest))
      ensure_dir(dest)
      shutil.copyfile(src, dest)

procedure = []
for foldname in glob.glob("0*/"):
  #if not(187 <= int(foldname.split('_')[0]) <= 336): continue
  if 'std_digitalscan' in foldname:
    procedure.append([])
  procedure[-1].append(foldname)

for tune in procedure:
  # files we haven't moved over because tune_threshold was negative
  folders_to_process = []
  tune_threshold = -1
  for foldname in tune:
    scan_threshold = -1
    data = json.load(open(os.path.join(foldname, 'scanLog.json')))

    if 'diff_tune_globalthreshold' in foldname:
      tune_threshold = data['targetCharge']

    if 'std_totscan' in foldname:
      scan_threshold = data['targetCharge']

    folders_to_process.append(foldname)
    base_destination = os.path.join('processed', 'tune_threshold_{0:d}'.format(tune_threshold))
    if tune_threshold > 0:
      for ifoldname in folders_to_process:
        destination = base_destination
        if 'std_totscan' in ifoldname:
          destination = os.path.join(destination, 'scan_threshold_{0:d}'.format(scan_threshold))
        move_pdfs(ifoldname, destination)
      folders_to_process = []

    print('\tTune Threshold: {0:d}\tScan Threshold: {1:d}'.format(tune_threshold, scan_threshold))
