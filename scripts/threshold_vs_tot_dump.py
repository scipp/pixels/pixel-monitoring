import glob
import json
import os

import sys
sys.path.insert(0, 'pixel-monitoring/scripts')
import fit_and_plot_tot as fapt

import numpy as np

procedure = []
for foldname in glob.glob("data/0*/"):
  #if not(187 <= int(os.path.basename(os.path.dirname(foldname)).split('_')[0]) <= 336): continue
  if 'std_digitalscan' in foldname:
    procedure.append([])
  procedure[-1].append(foldname)

mu_thresh = []
sigma_thresh = []
fit_data = []

for tune in procedure:
  # files we haven't moved over because tune_threshold was negative
  tune_threshold = -1
  for foldname in tune:
    injected_charge = -1
    data = json.load(open(os.path.join(foldname, 'scanLog.json')))

    if 'diff_tune_globalthreshold' in foldname:
      tune_threshold = data['targetCharge']

    if 'std_thresholdscan' in foldname:
      *_, fe_dif = fapt.read_dat(glob.glob(os.path.join(foldname, '*ThresholdMap-0.dat'))[0])
      fit_dif, err_dif, chi2_dif, dof_dif = fapt.fit_gaussian(fe_dif, bins=np.arange(0, 10000, 25))

      if fit_dif is None:
        print('\ttune: could not fit')
        continue
      print('\t\ttune: mu={0: 8.4f}±{2: 8.4f}  sigma={1: 8.4f}±{3: 8.4f}'.format(*fit_dif[1:], *np.sqrt(np.diag(err_dif))[1:]))
      mu_thresh.append(fit_dif[1])
      sigma_thresh.append(fit_dif[2])

    if 'std_totscan' in foldname:
      injected_charge = data['targetCharge']
      print('\tTune Threshold: {0:d}\tScan Threshold: {1:d}'.format(tune_threshold, injected_charge))
      *_, fe_dif = fapt.read_dat(glob.glob(os.path.join(foldname, '*MeanTotMap0.dat'))[0])

      fit_dif, err_dif, chi2_dif, dof_dif = fapt.fit_gaussian(fe_dif)
      if fit_dif is None:
        print('\t\tscan: could not fit')
        continue
      print('\t\tscan: mu={0: 8.4f}±{2: 8.4f}  sigma={1: 8.4f}±{3: 8.4f}'.format(*fit_dif[1:], *np.sqrt(np.diag(err_dif))[1:]))
      fit_data.append((tune_threshold, injected_charge, mu_thresh[-1], sigma_thresh[-1], fit_dif[1], fit_dif[2]))

json.dump(fit_data, open('fit_data.json', 'w+'), indent=4)
