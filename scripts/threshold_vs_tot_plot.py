import json
import numpy as np
from functools import reduce

fit_data = json.load(open('fit_data.json', 'r'))

import matplotlib.pyplot as pl

fit_data = np.array(fit_data)
plot_data = [[],[]]

#mask = reduce(np.bitwise_or, (fit_data[:,1] == i for i in np.arange(1000, 21000, 1000)))
#mask = np.bitwise_or(mask, fit_data[:,1] > 2*fit_data[:,0])
mask = fit_data[:,1] > 2*fit_data[:,0]

tune_threshold, injected_charge, mu_thresh, sigma_thresh, mu_tot, sigma_tot = fit_data[mask].T

fig, ax = pl.subplots()

#sc = ax.scatter(sigma_tot, sigma_thresh, c=injected_charge, s=50, alpha=0.5, cmap="Blues")
#sc = ax.scatter(sigma_tot, sigma_thresh, c=tune_threshold, s=50, alpha=0.5, cmap="Blues")
#sc = ax.scatter(sigma_tot, sigma_thresh, c=(injected_charge-tune_threshold), s=50, alpha=0.5, cmap="Blues")
#sc = ax.scatter(sigma_thresh, injected_charge, c=sigma_tot, s=50, alpha=0.5, cmap="Blues")
#sc = ax.scatter(tune_threshold, injected_charge, c=np.sqrt((sigma_thresh/tune_threshold)**2 + (sigma_tot/injected_charge)**2), s=20, alpha=0.5, cmap="Blues")
sc = ax.scatter(sigma_tot, mu_tot, c=sigma_thresh, s=50, alpha=0.5, cmap="Blues")
ax.set_xlabel(r'$\sigma_{\mathrm{ToT}}$ [b.c.]')
#ax.set_xlabel(r'$\sigma_{\mathrm{threshold}}$ [$e^{-}$]')
#ax.set_xlabel(r'tuned threshold [$e^{-}$]')
#ax.set_ylabel(r'injected charge [$e^{-}$]')
ax.set_ylabel(r'$\mu_{\mathrm{ToT}} [b.c.]$')
ax.set_title(r'Mask: injected_charge > (2 * tune_threshold)')

cbar = pl.colorbar(sc, ax=ax)
#cbar.set_label(r'injected threshold [$e^{-}$]')
#cbar.set_label(r'tuned threshold [$e^{-}$]')
#cbar.set_label(r'(tuned - injected) threshold [$e^{-}$]')
#cbar.set_label(r'$\sigma_{\mathrm{ToT}}$ [b.c.]')
#cbar.set_label(r'$\sqrt{\left(\frac{\sigma_{\mathrm{threshold}}}{\mathrm{threshold}}\right)^2 + \left(\frac{\sigma_{\mathrm{ToT}}}{\mathrm{ToT}}\right)^2}$')
cbar.set_label(r'$\sigma_{\mathrm{threshold}}$ [$e^{-}$]')
cbar.solids.set_edgecolor("face")  # https://stackoverflow.com/a/15021541
fig.show()
import pdb; pdb.set_trace()
