for tuneThreshold in $(seq 1000 1000 10000)
do
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/std_digitalscan.json -p -m 1
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/std_analogscan.json -p
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/diff_tune_globalthreshold.json -p -t $tuneThreshold
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/diff_tune_pixelthreshold.json -p -t $tuneThreshold
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/lin_tune_globalthreshold.json -p -t $(( $tuneThreshold + 1000 ))
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/lin_tune_pixelthreshold.json -p -t $(( $tuneThreshold + 1000 ))
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/lin_retune_globalthreshold.json -p -t $tuneThreshold
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/lin_retune_pixelthreshold.json -p -t $tuneThreshold
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/syn_tune_globalthreshold.json -p -t $tuneThreshold
  build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/std_thresholdscan.json -p
  for scanThreshold in $(seq 1000 1000 20000)
  do
    build/bin/scanConsole -r Yarr/src/configs/controller/specCfg.json -c Yarr/src/configs/connectivity/example_rd53a_setup.json -s Yarr/src/configs/scans/rd53a/std_totscan.json -p -t $scanThreshold
  done
done
