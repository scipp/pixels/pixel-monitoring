import numpy as np
import matplotlib.pyplot as pl
import glob
import scipy.optimize
import scipy.stats
import os

rowno = 192
colno = 400

def whichFE(col):
  division1 = 128
  division2 = 264
  which_fe = 10
  # synchronous
  if col < division1:
    which_fe = 0
  # linear
  elif division1 <= col < division2:
    which_fe = 1
  # differential
  elif col >= division2:
    which_fe = 2
  return which_fe

def filter_data(data, xmin=None, xmax=None):
  if xmin is not None and xmax is not None:
    return data[(data > xmin)&(data < xmax)]
  if xmin is not None and xmax is None:
    return data[data > xmin]
  elif xmin is None and xmax is not None:
    return data[data < xmax]
  else:
    return data

def gaus(x,a,mu,sigma):
  return a*np.exp(-(x-mu)**2/(2*sigma**2))

def fit_gaussian(data, bins=np.arange(17)):
  # bin data
  y, bin_edges = np.histogram(data, bins=bins)
  x = bin_edges[:-1] + np.diff(bin_edges)/2.
  # fit data
  try:
    fit_pars, err = scipy.optimize.curve_fit(gaus, x, y, p0=[y.max(),*scipy.stats.norm.fit(data)])
  except RuntimeError:
    return None, None, None, None
  try:
    # calculate chisquare
    chisquare, dof = test_compatibility(data, fit_pars)
  except ValueError:
    return fit_pars, err, None, None

  return fit_pars, err, chisquare, dof

def test_compatibility(data, fit_pars, bins=np.arange(17)):
  yobs, _ = np.histogram(data, bins=bins)
  yexp, _ = np.histogram(scipy.stats.norm.rvs(size=data.size, loc=fit_pars[1], scale=fit_pars[2]), bins=bins)
  mask = (yobs>5)&(yexp>5)
  return scipy.stats.chisquare(yobs[mask], yexp[mask]), np.sum(mask)

def plot_fit_results(data, fit_pars, fit_err, bins=np.arange(17), density=0):
  fig, ax = pl.subplots()
  _, _, hist = ax.hist(data, bins=bins+0.5, density=density, fill=False, histtype='step', zorder=0)

  legend_entries = [[hist[0]], ['data']]

  if fit_pars is not None:
    # figure out nominal, 1up, 1down
    fit_nom = gaus(bins, *fit_pars)
    fit_1up = gaus(bins, *(fit_pars + np.sqrt(np.diag(fit_err))))
    fit_1dn = gaus(bins, *(fit_pars - np.sqrt(np.diag(fit_err))))

    gaus_err = ax.fill_between(bins, fit_1dn, fit_1up, color='y', alpha=0.5, zorder=1)
    gaus_nom, = ax.plot(bins, fit_nom, color='k', linestyle='--', alpha=0.7, zorder=2)

    legend_entries[0].append((gaus_err, gaus_nom))
    legend_entries[1].append(r'gaus. fit $\pm 1\sigma$')

  ax.set_xlabel('Mean ToT [bc]')
  ax.set_ylabel('Number of Pixels')
  ax.legend(*legend_entries, loc=(0.735, 1.0), frameon=False)
  fig.tight_layout()
  return fig, ax

def read_dat(filename):
  with open(filename) as f:
    # skip first 8 lines (type, name, column, rows, hits, x-range, y-range)
    fdata = f.read().splitlines()
    hist_type = fdata[0]
    hist_name = fdata[1]
    data = np.fromstring(''.join(fdata[8:]), dtype=np.float64, sep=' ').reshape(-1, 400)
    fe_syn = data[:,:128].reshape(-1)
    fe_lin = data[:,128:264].reshape(-1)
    fe_dif = data[:,264:].reshape(-1)
  return hist_type, hist_name, fe_syn, fe_lin, fe_dif

if __name__ == "__main__":
  for filename in glob.glob("data/*/*MeanTotMap0.dat"):
    print(filename)

    *_, fe_syn, fe_lin, fe_dif = read_dat(filename)
    fit_syn, err_syn, chi2_syn, dof_syn = fit_gaussian(filter_data(fe_syn, xmin=2, xmax=14))
    fit_lin, err_lin, chi2_lin, dof_lin = fit_gaussian(filter_data(fe_lin, xmin=2, xmax=14))
    fit_dif, err_dif, chi2_dif, dof_dif = fit_gaussian(filter_data(fe_dif, xmin=2, xmax=14))

    print('SSE')
    if fit_syn is not None:
      print('\tsyn: mu={0: 8.4f}±{2: 8.4f}  sigma={1: 8.4f}±{3: 8.4f}'.format(*fit_syn[1:], *np.sqrt(np.diag(err_syn))[1:]))
    else: print('\tsyn: could not fit')
    if fit_lin is not None:
      print('\tlin: mu={0: 8.4f}±{2: 8.4f}  sigma={1: 8.4f}±{3: 8.4f}'.format(*fit_lin[1:], *np.sqrt(np.diag(err_lin))[1:]))
    else: print('\tlin: could not fit')
    if fit_dif is not None:
      print('\tdif: mu={0: 8.4f}±{2: 8.4f}  sigma={1: 8.4f}±{3: 8.4f}'.format(*fit_dif[1:], *np.sqrt(np.diag(err_dif))[1:]))
    else: print('\tdif: could not fit')

    fig, ax = plot_fit_results(fe_syn, fit_syn, err_syn)
    fig.savefig('{0:s}_syn.pdf'.format(os.path.basename(os.path.dirname(filename))))

    fig, ax = plot_fit_results(fe_lin, fit_lin, err_lin)
    fig.savefig('{0:s}_lin.pdf'.format(os.path.basename(os.path.dirname(filename))))

    fig, ax = plot_fit_results(fe_dif, fit_dif, err_dif)
    fig.savefig('{0:s}_dif.pdf'.format(os.path.basename(os.path.dirname(filename))))

    pl.close('all')
