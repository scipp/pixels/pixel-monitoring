# pixel-monitoring

A collection of submodules, scripts, and tools for performing the ITk Pixels work using the Yarr and RD53A for monitoring.

## Set up

### Cloning

To clone this repository, make sure you recursively clone to automatically get the submodules via

```
git clone --recursive ...
```

### Compiling

```
cmake3 -S Yarr -B /tmp/yarr-build
cmake3 --build /tmp/yarr-build -j4
cmake3 --install /tmp/yarr-build
```

And then `source setup.sh` which adds the `pixelmon/` python package into the python path

```
export PYTHONPATH=$PYTHONPATH:/path/to/pixel-monitoring
```

When all is compiled and set up, the built binaries and scripts will be located in `<path>/Yarr/bin/`.

### Configuring YARR

Follow the online documentation for other set-up (such as the kernel drivers): https://yarr.web.cern.ch/.

To do this correctly (instructions for use on the SCIPP machine), you first need to source the Xilinx Vivado settings shell script to set up environment variables for the vivado installation you're using. This is done with

```
source /opt/Xilinx/Vivado/2020.1/settings64.sh
```

Once that is done, you can flash the firmware using the provided script (see [Yarr documentation](https://yarr.web.cern.ch/yarr/pcie/)) which prompts for an option from the list of available bitstream files. Pick the one relevant for your board. For us at SCIPP, we want to use the following...

FPGA card:
```
0) tef1001_R1
1) tef1001_R2 <---- Trenz card, Revision 2
2) xpressk7_160
3) xpressk7_325
```

Chip type:
```
0) rd53 <---- RD53A/B modules
1) fei4
2) fe65p2
```

FMC card:
```
0) ohio <---
1) vhdci
```

Link speed:
```
0) 640Mbps
1) 160Mbps <----
```

Channel config:
```
0) 4x4 <---- 4 channels total, 4 lanes per channel
1) 16x1 <---- 16 channels total, 1 lane per channel
```

Notes:
- a DisplayPort handles 4 lanes of data
- a chip can read out up to 4 lanes of data
- a quad module can have 4 chips onto one DisplayPort (one lane per chip)
- a pseudoquad module can have 4 chips onto 4 DisplayPorts (up to four lanes per chip)

### Installing Drivers

On our pixdaq machines

```
cd /opt/Xilinx/Vivado/2020.1/data/xicom/cable_drivers/lin64/install_script/install_drivers/
sudo ./install_drivers
```

which gave output

```
[pixdaq@itkpix-daq-05 install_drivers]$ sudo ./install_drivers
INFO: Installing cable drivers.
INFO: Script name = ./install_drivers
INFO: HostName = itkpix-daq-05.ucsc.edu
INFO: Current working dir = /opt/Xilinx/Vivado/2020.1/data/xicom/cable_drivers/lin64/install_script/install_drivers
INFO: Kernel version = 3.10.0-1127.el7.x86_64.
INFO: Arch = x86_64.
USB udev file exists and will not be updated.
--File /etc/udev/rules.d/52-xilinx-ftdi-usb.rules exists.
--File /etc/udev/rules.d/52-xilinx-ftdi-usb.rules version = 0001
--File 52-xilinx-ftdi-usb.rules exists.
--File 52-xilinx-ftdi-usb.rules version = 0001
--File 52-xilinx-ftdi-usb.rules is already updated.
--File /etc/udev/rules.d/52-xilinx-pcusb.rules exists.
--File /etc/udev/rules.d/52-xilinx-pcusb.rules version = 0002
--File 52-xilinx-pcusb.rules exists.
--File 52-xilinx-pcusb.rules version = 0002
--File 52-xilinx-pcusb.rules is already updated.

INFO: Digilent Return code = 0
INFO: Xilinx Return code = 0
INFO: Xilinx FTDI Return code = 0
INFO: Return code = 0
INFO: Driver installation successful.
CRITICAL WARNING: Cable(s) on the system must be unplugged then plugged back in order for the driver scripts to update the cables.
```

# Documentation

- [Yarr docs](https://yarr.web.cern.ch/)

## Configuration Information

The Rigol DP821A has the mac address `00.19.af.5b.9b.bb`.

# Development

## pixelmon package

The `pixelmon` package is meant to provide utilities to automate certain portions of the pixel testing set up.

## Remotely Controlling Power


### Finding IP Address from MAC Address

From python, you can for example, find the IP address of the Rigol DP821A power supply knowing its mac address

```bash
[pixdaq@dhcp-130-148 ~]$ arp -n | grep 00.19.af.5b.9b.bb | awk '{print $1}'
128.114.130.75
```

or `nmap`

```bash
[pixdaq@dhcp-130-148 pixel-monitoring]$ sudo nmap -n -sP 128.114.130.0/24 | awk '/Nmap scan report/{printf $5;printf " ";getline;getline;print $3;}' | grep 00:19:AF:5B:9B:BB
128.114.130.75 00:19:AF:5B:9B:BB
```

and then use this IP address to directly connect with the power supply

```python
>>> from pixelmon import DP821A
>>> ps = DP821A("128.114.130.75")
>>> print ps
<DP821A@128.114.130.75(closed)>
>>> ps.open
>>> print ps
<DP821A@128.114.130.75(CH1=off) {"current": 1.0, "voltage": 0.0}>
```

or

```python
>>> from pixelmon import DP821A
>>> with DP821A("128.114.130.75") as ps:
...   print ps
...
<DP821A@128.114.130.75(CH1=off) {"current": 1.0, "voltage": 0.0}>
```

### Using Domain Name

There is currently one Rigol DP821A in the lab which is assigned to `pixelpsu-dp821a-1` with the IP address `128.114.130.75`. This means you can use the domain name directly with the power supply. Assuming you're within the UCSC campus network:

```python
>>> from pixelmon import DP821A
>>> ps = DP821A("pixelpsu-dp821a-1")
>>> print ps
<DP821A@pixelpsu-dp821a-1(closed)>
>>> ps.open
>>> print ps
<DP821A@pixelpsu-dp821a-1(CH1=off) {"current": 1.0, "voltage": 0.0}>
```

or

```python
>>> from pixelmon import DP821A
>>> with DP821A("pixelpsu-dp821a-1") as ps:
...   print ps
...
<DP821A@pixelpsu-dp821a-1(CH1=off) {"current": 1.0, "voltage": 0.0}>
```
