import json
import vxi11
import socket

class DP821A(vxi11.Instrument, object):

  def __init__(self, *args, **kwargs):
    super(DP821A, self).__init__(*args, **kwargs)
    self._channels = {1: False, 2: False}
    self._volt_min = {1: 0.0, 2: 0.0}
    self._volt_max = {1: 10, 2: 60}
    self._curr_min = {1: 0.0, 2: 0.0}
    self._curr_max = {1: 10, 2: 2}

  @property
  def open(self):
    super(DP821A, self).open()
    self.lock()
    self.reset

  @property
  def close(self):
    if self.locked: self.unlock()
    super(DP821A, self).close()

  @property
  def id(self):
    return self.ask("*IDN?")

  @property
  def channel(self):
    return int(self.ask(":INST:SELE?").split(":")[0][-1])

  @channel.setter
  def channel(self, value):
    if value not in self._channels: raise ValueError("Must select channel 1 or channel 2")
    self.write(":INST:SELE CH{0:d}".format(value))

  @property
  def opc(self):
    return self.ask("*OPC?")

  @property
  def reset(self):
    print('Resets are disabled for now.')
    #self.write("*RST")
    return self.opc

  @property
  def on(self):
    return self._toggler(True)

  @property
  def off(self):
    return self._toggler(False)

  @property
  def isOn(self):
    return bool(self.ask(":OUTP?") == 'ON')

  @property
  def isOff(self):
    return not(self.isOn)

  def _toggler(self, toggler):
    self.write(":OUTP {0:s}".format('ON' if toggler else 'OFF'))
    return self.opc

  @property
  def power(self):
    if self.isOn:
      return float(self.ask(":MEAS:VOLT?"))
    else:
      return float(self.ask(":POWE?"))

  @property
  def voltage(self):
    if self.isOn:
      return float(self.ask(":MEAS:VOLT?"))
    else:
      return float(self.ask(":VOLT?"))

  @property
  def current(self):
    if self.isOn:
      return float(self.ask(":MEAS:CURR?"))
    else:
      return float(self.ask(":CURR?"))

  @voltage.setter
  def voltage(self, value):
    minval = self._volt_min[self.channel]
    maxval = self._volt_max[self.channel]
    if not minval <= value <= maxval:
      raise ValueError("Must set voltage between {0:4f}V and {1:4f}V.".format(minval, maxval))
    self.write(":SOUR{0:d}:VOLT {1:0.4f}".format(self.channel, value))
    return self.opc

  @current.setter
  def current(self, value):
    minval = self._curr_min[self.channel]
    maxval = self._curr_max[self.channel]
    if not minval <= value <= maxval:
      raise ValueError("Must set current between {0:4f}A and {1:4f}A.".format(minval, maxval))
    self.write(":SOUR{0:d}:CURR {1:0.4f}".format(self.channel, value))
    return self.opc

  def currentVoltage(self):
    current, voltage = map(float, self.ask(":MEAS:CURR:DC? CH{0:d}".format(self.channel)).split(','))
    return {'current': current, 'voltage': voltage}

  def __repr__(self):
    if self.link is not None:
      return "<{0:s}@{4:s}(CH{2:d}={3:s}) {1:s}>".format(self.__class__.__name__, json.dumps({'current': self.current, 'voltage': self.voltage}, sort_keys=True), self.channel, 'on' if self.isOn else 'off', self.host)
    else:
      return "<{0:s}@{1:s}(closed)>".format(self.__class__.__name__, self.host)

  def __enter__(self):
    self.open
    return self

  def __exit__(self, exc_type, exc_value, traceback):
    self.close

  def __del__(self):
    self.close
