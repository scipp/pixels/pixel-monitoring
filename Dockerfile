FROM cern/cc7-base:latest

ADD Yarr /Yarr
ADD requirements.txt /home/pixdaq/

WORKDIR /home/pixdaq/

RUN yum install -y centos-release-scl devtoolset-7 \
                   cmake3 git gnuplot texlive-epstopdf \
                   zeromq zeromq-devel && \
    source /opt/rh/devtoolset-7/enable && scl enable devtoolset-7 bash && \
    cmake3 -S /Yarr -B /tmp/yarr-build && \
    cmake3 --build /tmp/yarr-build -j4 && \
    cmake3 --install /tmp/yarr-build && \
    echo 'PATH=/Yarr/bin:$PATH' >> /etc/profile.d/yarr.sh && \
    source /etc/profile.d/yarr.sh && \
    rm -rf /tmp/yarr-build && \
    yum clean all && \
    python3 -m pip install --no-cache-dir -r requirements.txt

COPY pixelmon /home/pixdaq/pixelmon
COPY scripts /home/pixdaq/scripts
COPY configs /home/pixdaq/configs
