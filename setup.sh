#!/bin/bash
if [ -z ${PIXELMON_SETUP+x} ]; then
  # add the Yarr to bin
  export PATH=/home/pixdaq/Yarr:$PATH
  # update pythonpath to add pixelmon/ package
  # figure out where this script is
  export PIXELMON_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  export PYTHONPATH=$PYTHONPATH:$PIXELMON_ROOT
  export PIXELMON_SETUP="true"
fi
